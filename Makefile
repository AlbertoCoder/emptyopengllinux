test:
	g++ -std=c++17 -c /home/alberto/Documents/C++Projects/Tutorial/src/main.cpp -o /home/alberto/Documents/C++Projects/Tutorial/bin/main.o -I /home/alberto/Documents/C++Projects/Tutorial/include
	g++ -std=c++17 -c /home/alberto/Documents/C++Projects/Tutorial/src/glad.c -o /home/alberto/Documents/C++Projects/Tutorial/bin/glad.o -I /home/alberto/Documents/C++Projects/Tutorial/include
	g++ /home/alberto/Documents/C++Projects/Tutorial/bin/main.o /home/alberto/Documents/C++Projects/Tutorial/bin/glad.o -o /home/alberto/Documents/C++Projects/Tutorial/main.exec -lglfw3 -lGL -lX11 -lpthread -lXrandr -lXi -ldl
	./main.exec