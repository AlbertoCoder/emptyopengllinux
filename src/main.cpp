#include "main.h"

int mode = 0;

int main()
{
    //This function allows us to initialize the GLFW framework and we're setting to use OPENGL_CORE
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    //This creates the window object but it DOESN'T draw it.
    GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    //GLAD initialization
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }
    //Set window dimension during drawing.
    glViewport(0, 0, 800, 600);
    //Setting which function to callback when resize event occurs.
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    //Endless draw loop every frame
    while(!glfwWindowShouldClose(window))
    {
        //Check for user input
        processInput(window);

        //Rendering Commands
        if(mode)
        {
            glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
        }else{
            glClearColor(0, 0, 0, 0);
            glClear(GL_COLOR_BUFFER_BIT);
        }


        glfwSwapBuffers(window);
        glfwPollEvents();    
    }
    //Function to free memory from all GLFW stuff
    glfwTerminate();
    return 0;
}

//Function that resizes the window. It gets called when resize event occurs
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

//Function that processes user input
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    //When spacebar is pressed the background color changes
    if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
    {
        if(mode == 1) mode = 0;
        else mode = 1;
    }
}