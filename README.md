# EmptyOpenGLLinux

Empty OpenGL Project for Linux users that use VSCode.
Used as base to learn from https://learnopengl.com/

This project can be freely used by anyone.
It's an empty project with only GLFW3 import.

Done for newbies of Ubuntu and VSCode.
